package com.dse.messagequeuemodel;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.Instant;


@Data
@Builder
public class AggregableValue {

    String identifier;
    Instant timeStamp;
    SensorTypeEnum type;
    BigDecimal value1;
    BigDecimal value2;
    BigDecimal value3;
}
