package com.dse.messagequeuemodel;

import lombok.Value;

import java.beans.ConstructorProperties;
import java.math.BigDecimal;

/**
 * DataRecord implementation describing a prediction for values that are likely to be recorded by air sensors in the near future.
 */
@Value
public class AirSensorPredictionDataRecord implements DataRecord {
    private BigDecimal airTemperature;
    private BigDecimal windSpeed;
    private BigDecimal humidity;

    @ConstructorProperties({"airTemperature", "windSpeed", "humidity"})
    public AirSensorPredictionDataRecord(BigDecimal airTemperature, BigDecimal windSpeed, BigDecimal humidity) {
        this.airTemperature = airTemperature;
        this.windSpeed = windSpeed;
        this.humidity = humidity;
    }
}
