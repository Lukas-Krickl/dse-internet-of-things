package com.dse.messagequeuemodel;

import lombok.Value;

/**
 * DataRecord describing an error in a microservice
 */
@Value
public class ErrorData implements DataRecord{
    private String originURL; //where the error was sent from
    private String cause; //where the error occurred
    private String message; // information on the error
}
