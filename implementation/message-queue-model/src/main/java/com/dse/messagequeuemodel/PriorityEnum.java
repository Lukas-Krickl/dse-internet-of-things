package com.dse.messagequeuemodel;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * the priority a message can have
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum PriorityEnum {
    LOW("LOW", 1),
    MEDIUM("MEDIUM", 2),
    HIGH("HIGH", 3);

    private String priority;
    @JsonIgnore
    private int value;

    /**
     * @param priority The case-insensitive string that will be mapped to the Enum
     * @return the enum corresponding to the string, or null if no match was found
     */
    @JsonCreator
    public static PriorityEnum fromType(@JsonProperty("priority") String priority) {
        return Arrays.stream(PriorityEnum.values())
                .filter(e -> e.priority.equalsIgnoreCase(priority))
                .findFirst().orElse(null);
    }
}