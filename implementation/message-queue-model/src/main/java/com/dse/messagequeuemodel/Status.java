package com.dse.messagequeuemodel;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * A status contains combined meta information about the message queue.<br>
 * It contains:
 * <ol>
 *     <li>total cache size</li>
 *     <li>amount of connected services</li>
 *     <li>amount of errors stored</li>
 * </ol>
 * <p>
 * Used by ms2 Monitoring component and cli-client
 */
public class Status {
    private int totalCacheSize;
    private int connectedServices;
    private int errorCount;

    @JsonCreator
    public Status(@JsonProperty("totalCacheSize") int totalCacheSize,
                  @JsonProperty("connectedServices") int connectedServices,
                  @JsonProperty("errorCount") int errorCount) {
        this.totalCacheSize = totalCacheSize;
        this.connectedServices = connectedServices;
        this.errorCount = errorCount;
    }

    public int getTotalCacheSize() {
        return totalCacheSize;
    }

    public int getConnectedServices() {
        return connectedServices;
    }

    public int getErrorCount() {
        return errorCount;
    }
}
