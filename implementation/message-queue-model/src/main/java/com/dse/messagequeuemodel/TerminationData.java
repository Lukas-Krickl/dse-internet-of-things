package com.dse.messagequeuemodel;

import lombok.Value;

import java.util.Set;

/**
 * Termination command
 */
@Value
public class TerminationData  implements DataRecord {
    /**
     * Set of all Subscriber(their URL) that should be terminating.
     * The String contains the URL at which the devices are reachable for example "http://192.168.134.23:8080"
     */
    private Set<String> targets;
}
