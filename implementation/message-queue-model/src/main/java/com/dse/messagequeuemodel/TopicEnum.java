package com.dse.messagequeuemodel;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * the possible topics of a message
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TopicEnum {
    PREDICTION("PREDICTION"),
    ANOMALY("ANOMALY"),
    METADATA("METADATA"),
    ERRORS("ERRORS"),
    TERMINATION("TERMINATION"),
    AIRSENSORDATA("AIRSENSORDATA"),
    WATERSENSORDATA("WATERSENSORDATA");

    private String topic;

    /**
     * @param topic The case-insensitive string that will be mapped to the Enum
     * @return the enum corresponding to the string, or null if no match was found
     */
    @JsonCreator
    public static TopicEnum fromType(@JsonProperty("topic") String topic
    ) {
        return Arrays.stream(TopicEnum.values())
                .filter(e -> e.topic.equalsIgnoreCase(topic))
                .findFirst().orElse(null);
    }
}
