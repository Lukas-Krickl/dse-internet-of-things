package com.dse.messagequeuemodel;

import lombok.Value;

import java.beans.ConstructorProperties;
import java.math.BigDecimal;

/**
 * DataRecord implementation describing a prediction for values that are likely to be recorded by water sensors in the near future.
 */
@Value
public class WaterSensorPredictionDataRecord implements DataRecord {

    private BigDecimal waterTemperature;
    private BigDecimal turbidity;
    private BigDecimal waveHeight;

    @ConstructorProperties({"waterTemperature", "turbidity", "waveHeight"})
    public WaterSensorPredictionDataRecord(BigDecimal waterTemperature, BigDecimal turbidity, BigDecimal waveHeight) {
        this.waterTemperature = waterTemperature;
        this.turbidity = turbidity;
        this.waveHeight = waveHeight;
    }
}
