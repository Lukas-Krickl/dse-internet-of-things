# MS1 Message Queue Documentation

## Structure
+ MessageQueueController receives http requests and passes
them to the MessageManager for to queueing messages into queues
or the SubscriptionManager for subscriber concerns.
+ Errors appearing in a controller's method are passed to the ExceptionHandler
+ MetaManager is used to record observed errors or to trace subscriber or cache metadata
+ MessageQueueRepository stores all application objects and classes
+ Queue contains a queue with messages of one subscriber
+ QueueExecutor sends messages from the Queue to the subscriber

## Microservices
+ A microservice has one Subscriber Object. 
    + The subscriber contains data/configuration of a microservice.
    + A subscriber is uniquely identified by its url
    + equals() compares the URL
    + checkIdentity() compares all fields
    + the subscriber object is used for blocking threads -> the object must not be replaced.
    for updating the whole object use the update(Subscriber) method or setters

+ A microservice has one Queue.
    + there is one queue for all topics
    + messages are ordered after priority and then after the timestamp
    + priorityqueue: the first/head element is ordered correctly, all other elements are not ordered
    + queue implements runnable: -> the run method sends the head of the queue to the MS
    + queue has the subscriber object to access url and delay needed for sending
    + if the queue is empty the thread waits until it is not (monitor pattern)
    + there are no idling queue threads

## Multithreading
the following objects have to be threadsafe in particular
+ Subscriber
+ Queue
+ MessageQueueRepository 

