package com.dse.messagequeue.controller;

import com.dse.messagequeue.manager.MessageManager;
import com.dse.messagequeue.manager.SubscriptionManager;
import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.requestwrapper.ChangeDelayRequestWrapper;
import com.dse.messagequeuemodel.requestwrapper.UnsubscribeRequestWrapper;
import org.springframework.web.bind.annotation.*;

@RestController
public class MessageQueueController {

    private final SubscriptionManager subscriptionManager;
    private final MessageManager messageManager;


    public MessageQueueController(MessageManager messageManager, SubscriptionManager subscriptionManager) {
        this.messageManager = messageManager;
        this.subscriptionManager = subscriptionManager;
    }

    @PostMapping("/subscriptions")
    public Subscriber subscribeToTopics(@RequestBody Subscriber subscriber) {
        return subscriptionManager.subscribeToTopics(subscriber);

    }

    @DeleteMapping("/subscriptions")
    public Subscriber unsubscribeFromTopics(@RequestBody UnsubscribeRequestWrapper request) {
        return subscriptionManager.unsubscribeFromTopics(
                request.getSubscriber(),
                request.getTopics()
        );
    }

    @PutMapping("/delays")
    public Subscriber updateDelay(@RequestBody ChangeDelayRequestWrapper request) {
        return subscriptionManager.updateDelay(
                request.getSubscriber(),
                request.getDelayInMillis()
        );

    }

    @PostMapping("/messages")
    public void publishMessage(@RequestBody Message message) {
        messageManager.processMessage(message);
    }
}
