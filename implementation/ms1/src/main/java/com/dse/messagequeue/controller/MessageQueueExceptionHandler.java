package com.dse.messagequeue.controller;

import com.dse.messagequeue.exception.InconsistentSubscriberException;
import com.dse.messagequeue.exception.InvalidInputException;
import com.dse.messagequeue.exception.SubscriberNotFoundException;
import com.dse.messagequeue.manager.MetaManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Exception handler which catches all exceptions thrown by a
 * method annotated with RequestMapping or any other kind of GetMapping PostMapping and so on
 * <p>
 * Each method catches the exceptions defined with the annotations and
 * returns a ResponseEntity which is sent back to the client as error code answer
 * <p>
 * The handler calls the most specific (best matching) method, as in a catch statement
 */
@ControllerAdvice
@Slf4j
public class MessageQueueExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {SubscriberNotFoundException.class})
    public final ResponseEntity<String> handleSubscriberNotFoundException(SubscriberNotFoundException e) {
        log.warn("Subscriber Not Found Error Occured", e);
        MetaManager.reportError(e);
        return new ResponseEntity<>("Subscriber '" + e.getSubscriberURL() + "' not Found", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {InconsistentSubscriberException.class})
    public final ResponseEntity<String> handleInconsistentSubscriberException(InconsistentSubscriberException e) {
        log.error("Inconsistent Subscriber", e);
        MetaManager.reportError(e);
        return new ResponseEntity<>("Inconsistency found in subscriber " + e.getMessageQueueSubscriber().getUrl(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {InvalidInputException.class})
    public final ResponseEntity<String> handleInvalidInputException(InvalidInputException e) {
        log.error("Invalid input from {} detected!", e.getSubscriberURL(), e);
        MetaManager.reportError(e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {RuntimeException.class})
    public final ResponseEntity<String> handleRuntimeException(RuntimeException e) {
        log.error("Unkown exception:", e);
        MetaManager.reportError(e);
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
