package com.dse.messagequeue.data;

import com.dse.messagequeue.exception.InconsistentSubscriberException;
import com.dse.messagequeue.exception.SubscriberNotFoundException;
import com.dse.messagequeue.manager.MetaManager;
import com.dse.messagequeue.manager.SubscriptionManager;
import com.dse.messagequeue.queue.Queue;
import com.dse.messagequeue.queue.QueueExecutor;
import com.dse.messagequeue.queue.QueueFactory;
import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.TopicEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.stream.Collectors;

/**
 * Threadsafe implementation of a Data Storage for subscribers and cachequeues.
 * As there is no real database in this application it acts as a MockImplementation holding data in memory.
 */
@Repository
public class MessageQueueRepository {
    private static final Logger log = LoggerFactory.getLogger(MessageQueueRepository.class);
    /**
     * Thread safe map of subscribers mapped to their URL
     */
    private final ConcurrentHashMap<String, Subscriber> subscribers;

    /**
     * Thread safe map of {@link Queue} mapped to the URL of the subscriber
     */
    private final ConcurrentHashMap<String, Queue> queues;
    /**
     * If one topic is not subscribed by any ms, the map contains an entry with the certain topic
     */
    private final ConcurrentHashMap<TopicEnum, PriorityBlockingQueue<Message>> caches;
    /**
     * Executor which runs the queues
     */
    private final QueueExecutor queueExecutor;
    private RestTemplate restTemplate;
    private SubscriptionManager subscriptionManager;

    /**
     * Creates the maps and fills cache with queues, as there are no subscribers at startup
     */
    public MessageQueueRepository(RestTemplate restTemplate, @Lazy SubscriptionManager subscriptionManager) {
        subscribers = new ConcurrentHashMap<>();
        queues = new ConcurrentHashMap<>();
        queueExecutor = new QueueExecutor();
        this.restTemplate = restTemplate;
        this.subscriptionManager = subscriptionManager;

        //cache all topics at initialisation
        caches = new ConcurrentHashMap<>();
        for (TopicEnum topic : TopicEnum.values()) {
            caches.put(topic, new PriorityBlockingQueue<>());
        }
    }

    /**
     * Updates a subscriber with the same url or adds a new one if none with this url exists
     * Checks if one topic
     *
     * @param subscriber the subscriber to add/update
     */
    public void createSubscriber(Subscriber subscriber) {
        Queue newQueue = QueueFactory.createQueue(subscriber, restTemplate, subscriptionManager);
        //add subscriber to subscribers list
        subscribers.put(subscriber.getUrl(), subscriber);
        //add queue to queue list
        queues.put(subscriber.getUrl(), newQueue);
        //execute queue's run to send messages
        queueExecutor.execute(subscriber.getUrl(), newQueue);
    }

    /**
     * Entirely removes a subscriber and his queue from the application
     *
     * @param subscriber subscriber to remove
     */
    public void deleteSubscriber(Subscriber subscriber) {
        //mark queue to terminate it self
        queues.get(subscriber.getUrl()).terminateExecution();
        //remove subscriber from subscriber list
        subscribers.remove(subscriber.getUrl());
        //remove queue from queues list
        queues.remove(subscriber.getUrl());
        //terminate queue externally and remove references on the runnable
        queueExecutor.terminateQueue(subscriber.getUrl());
    }

    public void addCachedMessagesToQueue(String subscriberUrl, TopicEnum topic) {
        queues.get(subscriberUrl).addMessages(caches.remove(topic));
    }


    /**
     * Validates the given subscriber on consistency with the stored subscriber
     *
     * @param clientSideSubscriber subscriber sent by a microservice
     * @throws SubscriberNotFoundException     if the given sub was not found
     * @throws InconsistentSubscriberException if the subscriber differ
     */
    public void validateSubscriberConsistency(Subscriber clientSideSubscriber) {
        if (!subscribers.containsKey(clientSideSubscriber.getUrl())) {
            throw new SubscriberNotFoundException(clientSideSubscriber.getUrl());
        }

        Subscriber mqSubscriber = subscribers.get(clientSideSubscriber.getUrl());
        if (!mqSubscriber.checkIdentity(clientSideSubscriber)) {
            throw new InconsistentSubscriberException("Subscribers are not equal but should!", mqSubscriber, clientSideSubscriber);
        }
    }

    // GETTERS & SETTERS

    /**
     * Returns the subscriber by its url
     *
     * @param key the url of the subscriber
     * @return the subscriber or null if it is not known
     */
    public Subscriber getSubscriberByUrl(String key) {
        return subscribers.get(key);
    }

    /**
     * Checks if a subscriber exists
     *
     * @param subscriberUrl subscriber
     * @return true if the subscriber exists, false otherwise
     */
    public boolean subscriberExists(String subscriberUrl) {
        return subscribers.containsKey(subscriberUrl);
    }

    /**
     * Returns the queue of a subscriber, or null if it is not existent
     *
     * @param url queue's subscriber
     * @return subscriber's queue
     */
    public Queue getQueueByURL(String url) {
        return queues.get(url);
    }

    /**
     * Returns all subscribers for a Topic
     *
     * @param topic the topic
     * @return a List<Subscriber> containing all subscribers or an empty list if none are found
     */
    public List<Subscriber> getSubscribersByTopic(TopicEnum topic) {
        return subscribers.values().stream()
                .filter(subscriber -> subscriber.getTopics().contains(topic))
                .collect(Collectors.toList());
    }

    /**
     * Returns the cachequeue of the passed topic or null it the queue does not exist yet
     *
     * @param topic the topic whose cache queue is wanted
     * @return a PriorityBlockingQueue<MessageTask> for the specified topic
     */
    public PriorityBlockingQueue<Message> getCacheQueueByTopic(TopicEnum topic) {
        return caches.get(topic);
    }


    public boolean isCached(TopicEnum topic) {
        return caches.containsKey(topic);
    }


    /**
     * Restores cache queues of the given topics if necessary
     * @param unsubscribedTopics topics that got newly unsubscribed
     */
    public void renewCacheQueue(Set<TopicEnum> unsubscribedTopics) {
        for (TopicEnum topic : unsubscribedTopics) {
            if (getSubscribersByTopic(topic).isEmpty()) {
                if (!caches.containsKey(topic)) {
                    caches.put(topic, new PriorityBlockingQueue<>());
                }
                MetaManager.cacheSizeUpdated(topic, 0);
            }
        }

    }




    /**
     * Logs the internal state and content of all maps of the repo
     */
    @Scheduled(fixedDelay = 5000, initialDelay = 5000)
    private void logInternalState() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n------------------------ Begin Internal Repository State ------------------------\n");
        sb.append("Subscribers: ").append(subscribers.size()).append(" |  Queues: ").append(queues.size()).append("  (should be equal)\n");

        // print subscribers
        sb.append("SUBSCRIBERS\n");
        for (String url : subscribers.keySet()) {
            Subscriber sub = subscribers.get(url);
            sb.append("+ ").append(url).append("  -- DELAY: ").append(sub.getDelayInMillis())
                    .append(" -- TOPICS: ").append(sub.getTopics().toString()).append("\n");
            sb.append("  QUEUESIZE: ").append(queues.get(url).size()).append("\n");
        }

        // print caches
        sb.append("CACHES\n");
        for (TopicEnum topic : caches.keySet()) {
            sb.append("+ ").append(topic.name()).append(" -- CacheSize: ").append(caches.get(topic).size()).append("\n");
        }


        sb.append("------------------------ End Internal Repository State ------------------------\n");
        log.debug(sb.toString());
    }
}
