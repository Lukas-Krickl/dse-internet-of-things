package com.dse.messagequeue.exception;

import com.dse.messagequeuemodel.Subscriber;

/**
 * This Exception should be thrown, if the Subscriber stored und the subscriber received differ, but should not
 * <p>
 * Now: if this happens and the exception is caught by the exception handler, an error response code is returned
 *
 */
public class InconsistentSubscriberException extends RuntimeException {
    private final Subscriber messageQueueSubscriber;
    private final Subscriber clientSideSubscriber;

    public InconsistentSubscriberException(String message, Subscriber messageQueueSubscriber, Subscriber clientSideSubscriber) {
        super(message);
        this.messageQueueSubscriber = messageQueueSubscriber;
        this.clientSideSubscriber = clientSideSubscriber;
    }

    public InconsistentSubscriberException(String message, Throwable cause, Subscriber messageQueueSubscriber, Subscriber clientSideSubscriber) {
        super(message, cause);
        this.messageQueueSubscriber = messageQueueSubscriber;
        this.clientSideSubscriber = clientSideSubscriber;
    }

    public Subscriber getMessageQueueSubscriber() {
        return messageQueueSubscriber;
    }

    public Subscriber getClientSideSubscriber() {
        return clientSideSubscriber;
    }
}
