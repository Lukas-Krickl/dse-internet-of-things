package com.dse.messagequeue.queue;


import com.dse.messagequeue.manager.SubscriptionManager;
import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.Subscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * A Queue contains:
 * <ul>
 * <li>the actual queue of messages (mixed topics)</li>
 * <li>the subscriber (for sending information)</li>
 * <li>maximum size to not overflow memory </li>
 * <li>and a termination flag to stop the sending thread</li>
 * </ul>
 * <p>
 * It implements runnable and its run method is called in the {@link QueueExecutor} which sends messages to the MS.
 * On setting the termination flag, the run method stops.
 * <p>
 * The Queue message sending mechanism is implemented using the signal and continue monitor pattern.
 * The sending thread waits if the queue does not contain messages.
 * When a other thread adds messages to the queue it notifies the waiting thread.
 * <p>
 * This should prevent repetitive checking on the queue´s size, which just blocks other threads from execution.
 */
public class Queue implements Runnable {

    /**
     * Queue of messages to be sent to a subscriber
     */
    private final PriorityBlockingQueue<Message> queue;

    /**
     * The Subscriber, the queue is from
     */
    private final Subscriber subscriber;
    /**
     * Maximum size of queue (used to prevent OutOfMemoryErrors). When this limit is reached and a message is added, the
     * latest message will be discarded in favor of the new one.
     */
    private final int maxQueueSize;

    /**
     * Flag for terminating the run method
     */
    private boolean shouldTerminate;

    private final int MAX_RETRIES = 10;
    private int connectionFailedRetry;

    private RestTemplate restTemplate;

    private final SubscriptionManager subscriptionManager;

    private Logger log = LoggerFactory.getLogger(Queue.class);

    public Queue(int maxQueueSize, Subscriber subscriber, RestTemplate restTemplate, SubscriptionManager subscriptionManager) {
        this.maxQueueSize = maxQueueSize;
        this.subscriber = subscriber;
        this.queue = new PriorityBlockingQueue<>(maxQueueSize / 2, Comparator.naturalOrder());
        this.shouldTerminate = false;
        this.connectionFailedRetry = MAX_RETRIES;
        this.restTemplate = restTemplate;
        this.subscriptionManager = subscriptionManager;
    }

    /**
     * Adds a new Message to the queue. If the queue reached its maximum capacity, a "random" message is deleted first.
     * "random" meaning messages are not sorted in any particular order, except the first
     * Notifies the waiting thread, that a message has been added.
     *
     * @param message new message to add
     */
    public void addMessage(Message message) {
        synchronized (queue) { //enter Monitor to notify
            while (queue.size() >= maxQueueSize) {
                queue.remove(queue.iterator().next()); //"best" solution for deleting a random message, possibly not being the head
                log.warn("Removing element from queue, due to reaching max Queue size of {} for queue {}", maxQueueSize, queue.size());
            }
            this.queue.offer(message);
            queue.notifyAll();
        }
    }

    /**
     * Adds a whole queue with messages to this queue.
     * Primarily intended for adding cached messages to this queue
     * <p>
     * If the queue would reach its maximum size, the necessary amount of messages is removed.
     * Notifies the waiting thread, that messages have been added.
     *
     * @param messages queue with messages, for example a cache queue
     */
    public void addMessages(PriorityBlockingQueue<Message> messages) {
        synchronized (queue) { //enter Monitor to notify
            int messagesToRemove = queue.size() + messages.size() - maxQueueSize;

            //removing all necessary elements from queue, to make space for new ones
            if (messagesToRemove > 0) {
                log.warn("Removing elements from queue, due to reaching max Queue size of {} for queue {}", maxQueueSize, queue.size());
                //to "throw away" messages, put it in an list, which is then garbage collected
                queue.drainTo(new ArrayList<>(), messagesToRemove);
            }

            queue.addAll(messages);
            queue.notifyAll();
        }
    }

    /**
     * Sets the termination flag
     */
    public synchronized void terminateExecution() {
        shouldTerminate = true;
    }

    public synchronized int size() {
        return queue.size();
    }

    /**
     * Enters the queue monitor and waits if the queue is empty.
     * After notification a message is sent to the client and delay is slept.
     * <p>
     * Checks after each iteration if the termination flag is set, and stops if it is so.
     */
    @Override
    public void run() {
        while (!shouldTerminate) {
            try {
                synchronized (queue) { //enter monitor to wait for notification
                    while (queue.isEmpty()) { //wait until there is a message to send
                        queue.wait();
                    }
                    //Send Message to microservice
                    log.debug("Sending message of queue " + subscriber.getUrl());
                    restTemplate.postForObject(subscriber.getUrl() + "/messages", queue.poll(), Message.class);

                    //connection established, restoring retries
                    if (connectionFailedRetry != MAX_RETRIES) {
                        connectionFailedRetry = MAX_RETRIES;
                    }
                }
                //sleep the delay before sending the next message
                Thread.sleep(subscriber.getDelayInMillis());

            } catch (InterruptedException e) {
                log.info("Queue from subscriber {} was interrupted: {}", subscriber.getUrl(), e.getMessage(), e);

            } catch (RestClientException e) {
                //connection failed
                if (connectionFailedRetry==0) {
                    //no retry left
                    log.warn("Connection retry is 0 killing service {}", subscriber.getUrl());
                    subscriptionManager.unsubscribeFromTopics(subscriber, subscriber.getTopics());
                } else {
                    connectionFailedRetry--;
                    log.warn("Failed to connect to Subscriber {} , retries left: {}"
                            , subscriber.getUrl(), connectionFailedRetry);
                }
            }
        }
        log.info("Queue from subscriber {} was terminated", subscriber.getUrl());
    }
}
