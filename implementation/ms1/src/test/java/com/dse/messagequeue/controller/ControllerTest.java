package com.dse.messagequeue.controller;

import com.dse.messagequeue.MQTestClassGenerator;

import com.dse.messagequeue.data.MessageQueueRepository;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.TopicEnum;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.context.SpringBootTest;

import java.util.EnumSet;


@SpringBootTest
public class ControllerTest {

	@Autowired
	private MessageQueueController controller;

	@Autowired
	private MessageQueueRepository repo;


	@Test
	void postSubscriptionsWithNewSubscriberReturnsNewSubscriber() {
		Subscriber testSubscriber = MQTestClassGenerator.getRandomSubscriber();
		Subscriber response = controller.subscribeToTopics(testSubscriber);

		Assertions.assertEquals(testSubscriber, response);
	}

	@Test
	void postSubscriptionExistingSubReturnsUpdatedSub() {
		//Initial subscriber
		Subscriber testSub = MQTestClassGenerator.getRandomSubscriber(EnumSet.of(TopicEnum.TERMINATION));
		testSub = controller.subscribeToTopics(testSub);

		//changed Subscriber (copied bc, reference stays the same without json serialization of web call
		testSub = new Subscriber(testSub.getUrl(), testSub.getDelayInMillis(), testSub.getTopics());
		testSub.setTopics(EnumSet.of(TopicEnum.TERMINATION, TopicEnum.AIRSENSORDATA, TopicEnum.PREDICTION));
		Subscriber response = null;
		try {
			response = controller.subscribeToTopics(testSub);
		} catch (Exception e){
			Assertions.fail();
		}

		Assertions.assertNotNull(response);
		Assertions.assertEquals(testSub, response);
	}

	@Test
	void postSubscriptionExistingInvalidSub_ReturnsUpdatedSub() {
		//Initial subscriber
		Subscriber testSub = MQTestClassGenerator.getRandomSubscriber(EnumSet.of(TopicEnum.TERMINATION));
		testSub = controller.subscribeToTopics(testSub);

		//changed Subscriber
		testSub = new Subscriber(testSub.getUrl(), testSub.getDelayInMillis(), testSub.getTopics());
		testSub.setTopics(EnumSet.of(TopicEnum.TERMINATION, TopicEnum.AIRSENSORDATA, TopicEnum.PREDICTION));

		Subscriber response = null;
		try {
			response = controller.subscribeToTopics(testSub);
		} catch (Exception e){
			Assertions.fail();
		}

		Assertions.assertNotNull(response);
		Assertions.assertEquals(testSub, response);
	}


}
