package io.github.lukas_krickl.cli_client.builder.application;

import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.exceptions.InvalidCommandConfigurationException;
import io.github.lukas_krickl.cli_client.logic.ApplicationManager;
import io.github.lukas_krickl.cli_client.visual.OutputText;

import java.util.Set;
import java.util.TreeSet;

public class CommandListBuilder {
	private OutputText configOutputText;
	private Set<Command> commandList;
	private Runnable initialisation;
	private Runnable shutdown;

	public CommandListBuilder(OutputText outputText, Runnable initialisation, Runnable shutdown) {
		commandList = new TreeSet<>();
		this.configOutputText = outputText;
		this.initialisation = initialisation;
		this.shutdown = shutdown;
	}

	public ApplicationManager build() {
		return new ApplicationManager(commandList, configOutputText, initialisation, shutdown);
	}

	public CommandListBuilder addCommand(Command cmd) {
		checkConfiguration(cmd);
		commandList.add(cmd);
		return this;
	}

	/**
	 * Checks if syntax or name does not already exist in list
	 * throws a {@link InvalidCommandConfigurationException} with the reason why it can't be injected
	 */
	private void checkConfiguration(Command other) {
		for (Command command : commandList) {
			//check if name already exists
			if (command.getCmdInfo().getName().equals(other.getCmdInfo().getName())) {
				throw new InvalidCommandConfigurationException("There is already a configured command with this name", command);
			}
			//check if syntax already exists
			for (String syntax : command.getCmdInfo().getSyntax()) {
				if (other.getCmdInfo().getSyntax().contains(syntax)) {
					throw new InvalidCommandConfigurationException("There is already a configured command with this", command);
				}
			}
		}
	}
}
