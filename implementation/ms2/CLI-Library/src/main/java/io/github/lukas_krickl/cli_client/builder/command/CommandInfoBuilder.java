package io.github.lukas_krickl.cli_client.builder.command;

import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.CommandInfo;

import java.util.HashSet;
import java.util.Hashtable;

/**
 * A builder for a CommandInfo object
 */
public class CommandInfoBuilder {
	private String name;
	private HashSet<String> syntax = new HashSet<>();
	private Hashtable<String, String> parameters = new Hashtable<>();
	private String description = "";
	private ArgsRequired requiresArgs = ArgsRequired.OPTIONAL;

	public CommandInfoBuilder(String name) {
		this.name = name.toUpperCase();
	}

	public CommandInfoBuilder addSyntax(String cmdCallingSyntax) {
		syntax.add(cmdCallingSyntax);
		return this;
	}

	public ParameterBuilder addParameterList() {
		return new ParameterBuilder(this);
	}

	protected CommandInfoBuilder setParameterList(Hashtable<String, String> parameters) {
		this.parameters = parameters;
		return this;
	}

	public CommandInfoBuilder setDescription(String description) {
		this.description = description;
		return this;
	}

	public CommandInfoBuilder setRequiresArgs(ArgsRequired isRequired) {
		requiresArgs = isRequired;
		return this;
	}


	public CommandInfo build() {
		return new CommandInfo(name, syntax, parameters, description, requiresArgs);
	}
}
