package io.github.lukas_krickl.cli_client.commands;

import java.util.TreeSet;

/**
 * Contains all configured commands
 */
public class CommandRepository {
	private TreeSet<Command> definedCommands;

	public CommandRepository(TreeSet<Command> definedCommands) {
		this.definedCommands = definedCommands;
	}

	public TreeSet<Command> getDefinedCommands() {
		if (definedCommands == null) {
			throw new NullPointerException("Defined Commands need to be set up");
		}
		return new TreeSet<>(definedCommands);
	}
}
