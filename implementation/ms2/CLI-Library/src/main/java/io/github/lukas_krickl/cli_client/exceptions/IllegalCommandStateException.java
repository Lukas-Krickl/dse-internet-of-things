package io.github.lukas_krickl.cli_client.exceptions;

public class IllegalCommandStateException extends Exception {
	public IllegalCommandStateException(String message) {
		super(message);
	}

	public IllegalCommandStateException(String message, Throwable cause) {
		super(message, cause);
	}

	public IllegalCommandStateException(Throwable cause) {
		super(cause);
	}

	public IllegalCommandStateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
