package io.github.lukas_krickl.cli_client.exceptions;

import io.github.lukas_krickl.cli_client.commands.Command;

import java.util.Optional;

public class InvalidCommandConfigurationException extends RuntimeException {
	private Optional<Command> illegalCommand;

	public InvalidCommandConfigurationException(String message) {
		super(message);
		illegalCommand = Optional.empty();
	}

	public InvalidCommandConfigurationException(String message, Command illegalCommand) {
		super(message);
		this.illegalCommand = Optional.of(illegalCommand);
	}

	public InvalidCommandConfigurationException(String message, Throwable cause, Command illegalCommand) {
		super(message, cause);
		this.illegalCommand = Optional.of(illegalCommand);
	}

	public Optional<Command> getIllegalCommand() {
		return illegalCommand;
	}
}
