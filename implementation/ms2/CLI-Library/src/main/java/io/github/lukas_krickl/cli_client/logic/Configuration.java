package io.github.lukas_krickl.cli_client.logic;

import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.commands.general.*;

/**
 * Class for central configuration
 */
public class Configuration {
    /**
     * Commands configured per default
     */
    public static final Command[] GENERAL_COMMANDS = {
            new ColorMode(),
            new Help(),
            new Licence(),
            new Quit(),
            new Version()
    };

    /**
     * Amount of characters of the screen width
     */
    public static final int SCREEN_WIDTH = 80;
}
