package io.github.lukas_krickl.cli_client.logic;

import io.github.lukas_krickl.cli_client.commands.CommandRepository;
import io.github.lukas_krickl.cli_client.exceptions.InputCanceledException;
import io.github.lukas_krickl.cli_client.visual.OutputText;

import java.util.Scanner;

/**
 * Class as communication interface between user and application:
 * every input and output should be made over this class to centralize functionality
 * <p>
 * Implements singleton pattern
 */
public class UserInterface {

    /**
     * Instance of singleton pattern
     */
    private static UserInterface instance = null;

    /**
     * Scanner for reading user input
     */
    private final Scanner sc = new Scanner(System.in);

    /**
     * Output text the basic output is printed from
     */
    private OutputText outputText;

    /**
     * Collection of all configured commands
     */
    private CommandRepository cmdRepo;


    private UserInterface(OutputText outputText, CommandRepository cmdRepo) {
        this.outputText = outputText;
        this.cmdRepo = cmdRepo;
    }

    /**
     * @return the User interface
     */
    public static UserInterface getInstance() {
        if (instance == null) {
            throw new NullPointerException("UserInterface is not set up yet!");
        }
        return instance;
    }

    /**
     * Used by the {@link ApplicationManager} to set up the user interface, can be used for unit testing
     *
     * @param outputText Application specific OutputText
     * @param cmdRepo    Collection of all configured commands
     * @return the user interface instance
     */
    public static UserInterface setUp(OutputText outputText, CommandRepository cmdRepo) {
        if (instance == null) {
            instance = new UserInterface(outputText, cmdRepo);
        }
        return instance;
    }

    // USER INTERFACE UTILITY METHODS FOR USER INTERACTION

    /**
     * Uses per default the System.out.print() method
     *
     * @param output String to print on screen
     */
    public static void print(String output) {
        System.out.print(output);
    }

    /**
     * Uses per default the System.out.println() method
     *
     * @param output String to print on screen
     */
    public static void println(String output) {
        System.out.println(output);
    }

    /**
     * Prints the text with a new line at the beginning and at the end like "\n"+output+"\n\n"
     * <p>
     * Uses per default the System.out.println() method
     *
     * @param output String to print on screen
     */
    public static void printSection(String output) {
        System.out.println("\n" + output + "\n");
    }

    /**
     * Waits until the user presses enter without displaying any output
     */
    public void getUserConfirmation() {
        sc.nextLine();
    }

    /**
     * Asks the user to confirm with a 'y' or to decline with a 'n'
     *
     * @param question Question to ask the user for confirmation
     * @return true if y, false if n was inserted, loops otherwise
     */
    public boolean getUserConfirmationDialog(String question) {
        String answer;
        do {
            println(question + " [y/n]");
            answer = sc.nextLine().trim().toLowerCase();
        } while (!(answer.equals("y") || answer.equals("n")));

        return answer.equals("y");
    }

    /**
     * Asks the user for a line of input without any output displayed to the screen
     *
     * @return the line as string the user inserted
     */
    public String getNextLine() {
        print(outputText.getInputPrefix());
        return sc.nextLine();
    }

    /**
     * Requests a not empty line from the user
     *
     * @return returns a not empty string from the user
     * @throws InputCanceledException if the cancel syntax as specified in the configured {@link OutputText} was inserted
     */
    public String getNextNotEmptyLine() throws InputCanceledException {
        String inputLine;

        while (true) {
            print(outputText.getInputPrefix());
            inputLine = sc.nextLine();

            if (!inputLine.isEmpty()) {
                if (inputLine.equals(outputText.getCancelUserInputSyntax())) {
                    throw new InputCanceledException();
                }
                return inputLine;
            }
        }
    }

    //USER INTERFACE CONTROL METHODS

    /**
     * Used by the {@link ApplicationManager} at startup to display initialisation output
     */
    protected void initialize() {
        println(outputText.getApplicationLogo());
        println(outputText.getVersion());
        println(outputText.getLicence());
        println(outputText.initialisationText());
    }

    /**
     * Used by the {@link ApplicationManager} to close the input stream on application termination
     */
    protected void exit() {
        sc.close();
    }

    // GETTERS & SETTERS
    public void setOutputText(OutputText outputText) {
        this.outputText = outputText;
    }

    public OutputText getOutputText() {
        return outputText;
    }

    public CommandRepository getCmdRepo() {
        return cmdRepo;
    }
}
