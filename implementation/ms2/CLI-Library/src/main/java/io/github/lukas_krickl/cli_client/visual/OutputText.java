package io.github.lukas_krickl.cli_client.visual;

/**
 * Interface for printing basic application output
 */
public interface OutputText {
	/**
	 * @return Returns the Application name/logo
	 */
	String getApplicationLogo();

	/**
	 * @return Licence text of this application
	 */
	String getLicence();

	/**
	 * @return version of the application
	 */
	String getVersion();

	/**
	 * @return Text which is displayed at application startup
	 */
	String initialisationText();

	/**
	 * @return Returns the commandline prefix displayed on user input e.g. "appName > "
	 */
	String getInputPrefix();

	default String getHelpDialog() {
		return "  _   _ _____ _     ____  \n" +
				" | | | | ____| |   |  _ \\ \n" +
				" | |_| |  _| | |   | |_) |\n" +
				" |  _  | |___| |___|  __/ \n" +
				" |_| |_|_____|_____|_|    \n" +
				"\nGENERAL INFORMATION:\n" +
				"To cancel input type '-c'" +
				"\n";
	}

	default String getCancelUserInputSyntax() {
		return "-c";
	}


}
