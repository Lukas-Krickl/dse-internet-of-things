package com.dse.monitoring_component.cli_client.commands;

import com.dse.messagequeuemodel.Status;
import com.dse.monitoring_component.cli_client.data.DAO;

import com.dse.monitoring_component.cli_client.logic.NetworkConnector;
import com.dse.monitoring_component.cli_client.visual.Display;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;
import io.github.lukas_krickl.cli_client.visual.TextColor;

import java.io.IOException;

/**
 * Command to display combined meta information
 */
public class StatusCommand extends Command {
    public StatusCommand() {
        super(new CommandInfoBuilder("STATUS")
                .addSyntax("status")
                .setDescription("Displays combined status information of the message queue.\n" +
                        "Is refreshed at configured refresh rate.")
                .setRequiresArgs(ArgsRequired.NOT_REQUIRED)
                .build());
    }

    @Override
    public void run() {
        UserInterface ui = UserInterface.getInstance();
        Thread displayStatus = new Thread(this::displayStatus);

        //Start thread to continuously display status
        displayStatus.start();
        //listen for user to press enter to stop the thread
        ui.getUserConfirmation();
        //stop the thread
        displayStatus.interrupt();
    }

    /**
     * Displays the received status repeatedly
     * This method is designed to run in its own thread an has to be stopped using interrupt
     */
    private void displayStatus() {
        NetworkConnector connector = DAO.getInstance().getConnector();
        int refreshRate = DAO.getInstance().getRefreshRate();
        try {
            while (true) {
                Status status = connector.requestStatus();
                UserInterface.print(Display.formatStatus(status) + "\r");
                Thread.sleep(refreshRate);
            }

        } catch (IOException e) {
            UserInterface.printSection(TextColor.RED.apply("ERROR: ")+e.getMessage());

        } catch (InterruptedException ignored) {} //when user presses enter
    }


}
