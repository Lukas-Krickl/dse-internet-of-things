package com.dse.monitoring_component.cli_client.visual;

import io.github.lukas_krickl.cli_client.visual.OutputText;

/**
 * Implementation of the OutputText interface of the CLI-Library
 */
public class ClientOutputText implements OutputText {
    public ClientOutputText() {
    }

    @Override
    public String getApplicationLogo() {
        return "  __  __  _____ ___      _____ _      _____             _____ _ _            _   \n" +
                " |  \\/  |/ ____|__ \\    / ____| |    |_   _|           / ____| (_)          | |  \n" +
                " | \\  / | (___    ) |  | |    | |      | |    ______  | |    | |_  ___ _ __ | |_ \n" +
                " | |\\/| |\\___ \\  / /   | |    | |      | |   |______| | |    | | |/ _ \\ '_ \\| __|\n" +
                " | |  | |____) |/ /_   | |____| |____ _| |_           | |____| | |  __/ | | | |_ \n" +
                " |_|  |_|_____/|____|   \\_____|______|_____|           \\_____|_|_|\\___|_| |_|\\__|\n";
    }

    @Override
    public String getLicence() {
        return "    Licence: GPL v3\n" +
                "    This program is free software: you can redistribute it and/or modify\n" +
                "    it under the terms of the GNU General Public License as published by\n" +
                "    the Free Software Foundation, either version 3 of the License, or\n" +
                "    (at your option) any later version.\n" +
                "\n" +
                "    This program is distributed in the hope that it will be useful,\n" +
                "    but WITHOUT ANY WARRANTY; without even the implied warranty of\n" +
                "    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n" +
                "    GNU General Public License for more details.\n" +
                "\n" +
                "    You should can access a copy of the GNU General Public License\n" +
                "    here: <https://www.gnu.org/licenses/>.\n\n";
    }

    @Override
    public String getVersion() {
        return "    Version: 1.5\n";
    }

    @Override
    public String initialisationText() {
        return "";
    }

    @Override
    public String getInputPrefix() {
        return "ms2-client > ";
    }
}
