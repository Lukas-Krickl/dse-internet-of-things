package com.dse.monitoring_component.cli_client.visual;

import com.dse.messagequeuemodel.*;
import com.dse.monitoring_component.cli_client.data.Configuration;
import io.github.lukas_krickl.cli_client.visual.TextColor;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A collection of utility methods to format output printed to the screen
 */
public class Display {
    public static Spinner spinner = Spinner.THIRD;

    /**
     * defined maximum length of the output in characters
     */
    public static int screenLength = Configuration.defaultScreenLength;


    public static String nextSpinner() {
        spinner = spinner.getNext();
        return spinner.getSymbol();
    }


    /**
     * Formats a status to be displayed on screen
     *
     * @param status status from the MC
     * @return formatted status
     */
    public static String formatStatus(Status status) {
        return textSpaceBetween("[Connected-Services: " + status.getConnectedServices() + "]",
                "[Errors: " + status.getErrorCount() + "]",
                "[Total-Cache-Size: " + status.getTotalCacheSize() + "] " + nextSpinner());
    }

    /**
     * Returns a string with cache sizes ready to print on screen
     *
     * @param cache cache to display
     * @return string ready for displaying
     */
    public static String formatCache(Map<TopicEnum, Integer> cache) {
        screenLength/=2;
        StringBuilder sb = new StringBuilder("\n\n");
        sb.append(textFillLine("=", screenLength)).append("\n"); //=====line
        sb.append(textCenter("CACHE SIZE OF TOPICS")).append("    ").append(nextSpinner());

        for (TopicEnum topic : cache.keySet()) {
            String cacheSize = (cache.get(topic) ==-1) ? "not-cached":cache.get(topic).toString();
            sb.append("\n").append(textSpaceBetween(topic.name(), cacheSize).replace(" ", "."));
        }
        sb.append("\n").append(textFillLine("=", screenLength)).append("\n");
        screenLength*=2;
        return sb.toString();
    }

    /**
     * Returns a string with error messages ready to print on screen
     *
     * @return string ready to print
     */
    public static String formatErrorMessages(List<Message> errors) {
        StringBuilder sb = new StringBuilder("\n\n");
        sb.append(textCenter("ERROR MESSAGES"));

        for (int i = errors.size(); i > 0; i--) {
            ErrorData data = (ErrorData) errors.get(i - 1).getRecord();

            sb.append("\n").append(TextColor.CYAN.apply(String.format("[%2d] ", i))).append(textFillLine("=", screenLength - 5)); //linenumber [1]
            sb.append("\n  ORIGIN:    ").append(TextColor.BRIGHT_BLUE.apply(data.getOriginURL())); //Origin url
            sb.append("\n  CAUSE:     ").append(TextColor.YELLOW.apply(data.getCause())); //cause url
            sb.append("\n  TIMESTAMP: ").append(String.format("%tc", Date.from(errors.get(i - 1).getTimestamp()))); //message timestamp
            sb.append("\n  MESSAGE:   ").append(TextColor.RED.apply(textBlock(13, data.getMessage())));
        }

        return sb.toString();
    }

    /**
     * Formats a subscriber list to be printed on screen
     *
     * @param subList list with services
     * @return formatted string
     */
    public static String formatServices(Set<Subscriber> subList) {
        StringBuilder sb = new StringBuilder("\n\n");

        sb.append(textFillLine("=", screenLength)).append("\n");
        sb.append(textCenter("CONNECTED SERVICES")).append("    ").append(nextSpinner());
        int i = 1;
        for (Subscriber sub : subList) {
            sb.append("\n").append(String.format("[%2d] ", i++)).append(textFillLine("=", screenLength - 5));
            sb.append("\n  URL:   ").append(TextColor.BRIGHT_BLUE.apply(sub.getUrl()));
            sb.append("\n  DELAY: ").append(sub.getDelayInMillis());
            sb.append("\n  TOPICS:").append(textBlock(7, sub.getTopics().toString()));
        }

        return sb.toString();
    }

    /**
     * Formats to a new "page", moves older displayed content far up
     *
     * @return multiple \n to hide upper output
     */
    public static String formatToNewPage(){
        return "\n\n\n\n\n\n";
    }




    //             FORMATTING UTILITY METHODS
    ///////////////////////////////////////////////////////////////////

    /**
     * Centers a text horizontally using whitespaces
     * @param text text to center
     *
     * @return centered string
     */
    private static String textCenter(String text) {
        int begin = (screenLength + text.length()) / 2;
        return String.format("%" + begin + "s", text);
    }

    /**
     * Prints a text on the left side of the screen in the given minimum width and filling with whitespaces
     *
     * @param text text to place
     * @param width minimum width of the string, filled with whitespaces
     * @return formatted string
     */
    private static String textLeft(String text, int width) {
        return String.format("%" + width + "s", text);
    }

    /**
     * Prints a text on the right side of the screen in the given minimum width and filling with whitespaces
     *
     * @param text text to place
     * @param width minimum width of the string, filled with whitespaces
     * @return formatted string
     */
    private static String textRight(String text, int width) {
        return String.format("%-" + width + "s", text);
    }

    /**
     * Fills a string of the given length with the given chars
     * @param c chars to fill with
     * @param length length of the string
     * @return formatted string
     */
    private static String textFillLine(String c, int length) {
        return String.format("%" + length + "s", c).replace(" ", c);
    }

    /**
     * Formats a text to a block of the set screenLength. Words are truncated
     *
     * @param borderLeft margin on the left in whitespaces
     * @param text text to format
     * @return formatted text
     */
    private static String textBlock(int borderLeft, String text) {
        if (text== null) {
            text = "";
        }

        StringBuilder sb = new StringBuilder();

        String[] lines = text.split("(?<=\\G.{" + (screenLength - borderLeft) + "})");

        sb.append(lines[0]).append("\n");
        for (int i = 1; i < lines.length; i++) {
            sb.append(String.format("%" + borderLeft + "s", "")).append(lines[i]).append("\n");
        }
        return sb.toString();
    }

    /**
     * Formats the given strings that all are placed in line and
     * there is even space between each string
     *
     * @param text words to place
     * @return formatted string
     */
    private static String textSpaceBetween(String... text) {
        StringBuilder sb = new StringBuilder();
        int spaceBetween = screenLength;
        //subtract all chars of text
        for (String s : text) {
            spaceBetween -= s.length();
        }
        spaceBetween /= text.length - 1; //divide per text
        spaceBetween = Math.max(spaceBetween, 1); //assure not negative

        String space = String.format("%" + spaceBetween + "s", "");


        sb.append(text[0]);
        for (int i = 1; i < text.length; i++) {
            sb.append(space).append(text[i]);
        }
        return sb.toString();
    }

}
