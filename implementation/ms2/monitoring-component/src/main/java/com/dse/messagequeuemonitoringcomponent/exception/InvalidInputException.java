package com.dse.messagequeuemonitoringcomponent.exception;

/**
 * Meant to be thrown if an request parameter was invalid.
 * ErrorHandler will return a code 400
 */
public class InvalidInputException extends RuntimeException {
    public InvalidInputException(String message) {
        super(message);
    }

    public InvalidInputException(String message, Throwable cause) {
        super(message, cause);
    }
}
