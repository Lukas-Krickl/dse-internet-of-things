package com.dse.messagequeuemonitoringcomponent.exception;

import com.dse.messagequeuemodel.Message;

/**
 * Thrown when inconsistency in a message is detected,
 * for example a message in the wrong topic
 *
 */
public class InvalidMessageReceivedException extends RuntimeException {
    private Message mqMessage;

    public InvalidMessageReceivedException(String message, Message mqMessage) {
        super(message);
        this.mqMessage = mqMessage;
    }

    public InvalidMessageReceivedException(String message, Throwable cause, Message mqMessage) {
        super(message, cause);
        this.mqMessage = mqMessage;
    }

    public Message getMqMessage() {
        return mqMessage;
    }
}
