package com.dse.messagequeuemonitoringcomponent.exception;

/**
 * Thrown when information is requested by the cli client but the
 * connection to the message queue isn´t even established.
 */
public class MQConnectionFailedException extends RuntimeException {
    private static final String defaultErrorMessage = "Data is not available:\n" +
            "    +) The monitoring component didn't connect to the message queue yet\n" +
            "    +) or it is unreachable!";

    public MQConnectionFailedException() {
        super(defaultErrorMessage);
    }
}
