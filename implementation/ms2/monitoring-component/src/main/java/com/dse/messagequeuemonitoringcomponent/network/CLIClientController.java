package com.dse.messagequeuemonitoringcomponent.network;


import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.Status;
import com.dse.messagequeuemodel.Subscriber;
import com.dse.messagequeuemodel.TopicEnum;
import com.dse.messagequeuemonitoringcomponent.managers.ApplicationManager;
import com.dse.messagequeuemonitoringcomponent.managers.DataManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Defines REST Endpoints for accessing managed data
 * For communication with the cli client
 */
@RestController
public class CLIClientController {

    private static final Logger log = LoggerFactory.getLogger(CLIClientController.class);
    private ApplicationManager applicationManager;
    private DataManager dataManager;

    @Autowired
    public CLIClientController(ApplicationManager applicationManager, DataManager dataManager) {
        this.applicationManager = applicationManager;
        this.dataManager = dataManager;
    }

    @GetMapping("/status")
    public Status getStatus() {
        return dataManager.requestStatus();
    }

    @GetMapping("/status/cache")
    public Map<TopicEnum, Integer> getCacheSize() {
        return dataManager.requestCacheSize();
    }

    @GetMapping("/services")
    public Set<Subscriber> getConnectedServices() {
        return dataManager.requestServices();
    }

    @DeleteMapping("/services")
    public ResponseEntity<?> terminateServices(@RequestBody String[] serviceURLS) {
        applicationManager.terminateOther(serviceURLS);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/messages/errors")
    public List<Message> getErrors(@RequestParam(value = "count", required = false) Integer count) {
        return dataManager.requestErrors(count);
    }

    @DeleteMapping("/messages/errors")
    public ResponseEntity<?> clearErrors(@RequestParam(value = "size", required = false) Integer size) {
        dataManager.requestClearErrors(size);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}
