package com.dse.messagequeuemonitoringcomponent.managers;

import com.dse.messagequeuemodel.CacheMeta;
import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.TopicEnum;
import com.dse.messagequeuemonitoringcomponent.managers.messages.CacheMessageManager;
import com.dse.messagequeuemonitoringcomponent.TestDataGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class CacheMessageManagerTest {
	private DataManager dataManager;
	private CacheMessageManager cacheMessageManager;

	@BeforeEach
	void setUp() {
		dataManager = TestDataGenerator.generateDataManagerWithData();
		cacheMessageManager = new CacheMessageManager(dataManager);
	}

	@Test
	void callManageCacheWithNewMessage_dataManagerContainsUpdateFromMessage() {
		int newCacheSize = 666;
		TopicEnum topic = TopicEnum.ERRORS;

		assertNotEquals(newCacheSize, dataManager.getCacheSize().get(topic).getCacheSize());

		CacheMeta data = new CacheMeta(topic, newCacheSize);
		cacheMessageManager.manageCache(new Message(topic, data));

		assertEquals(newCacheSize, dataManager.getCacheSize().get(topic).getCacheSize());
	}

	@Test
	void callManageCacheWithOldMessage_dataManagerIsNotAffected() {
		int newCacheSize = 666;
		TopicEnum topic = TopicEnum.ERRORS;
		int oldCacheSize = dataManager.getCacheSize().get(topic).getCacheSize();

		assertNotEquals(newCacheSize, oldCacheSize);

		CacheMeta data = new CacheMeta(topic, newCacheSize);
		Message oldMessage = new Message(topic, data);
		ReflectionTestUtils.setField(oldMessage, "timestamp", Instant.MIN, Instant.class);

		cacheMessageManager.manageCache(oldMessage);

		assertEquals(oldCacheSize, dataManager.getCacheSize().get(topic).getCacheSize());
	}
}