package com.dse.test_microservice_client.application;


import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeuemodel.Subscriber;

import java.util.Hashtable;

/**
 * Stores all classes needed by commands
 */
public class TestClientRepository {
    private static final TestClientRepository instance = new TestClientRepository();

    private String mqURL = "";
    private MessageQueueClient mqConnection = null;
    private Hashtable<Subscriber, MessageQueueClient> subscribers = new Hashtable<>();

    private TestClientRepository() {
    }

    public static TestClientRepository getInstance() {
        return instance;
    }

    public void configureMQUrl(String mqURL) {
        this.mqURL = mqURL;
        this.mqConnection = new MessageQueueClient(Configuration.TEST_CLIENT_URL, mqURL);
    }

    public boolean isMQUrlConfigured() {
        return mqConnection != null;
    }

    public String getMessageQueueURL() {
        return mqURL;
    }

    public MessageQueueClient getMqConnection() {
        return mqConnection;
    }

    public Hashtable<Subscriber, MessageQueueClient> getSubscribers() {
        return subscribers;
    }

    /**
     * Returns subscriber with given number
     * Returns null if subscriber does not exist
     *
     * @param number
     * @return
     */
    public Subscriber findSubscriberByNumber(int number) {
        int i = 0;
        for (Subscriber sub : subscribers.keySet()) {
            if (i == number) {
                return sub;
            }
            i++;
        }
        return null;
    }

    public Subscriber findSubscriberByURL(String url) {
        for (Subscriber s : subscribers.keySet()) {
            if (s.getUrl().equals(url)) {
                return s;
            }
        }
        return null;
    }
}
