package com.dse.test_microservice_client.commands;

import com.dse.test_microservice_client.application.TestClientRepository;
import io.github.lukas_krickl.cli_client.builder.command.CommandInfoBuilder;
import io.github.lukas_krickl.cli_client.commands.ArgsRequired;
import io.github.lukas_krickl.cli_client.commands.Command;
import io.github.lukas_krickl.cli_client.logic.UserInterface;

public class ConfigMQUrlCommand extends Command {

    public ConfigMQUrlCommand() {
        super(new CommandInfoBuilder("CONFIGURE MQ URL")
                .addSyntax("config")
                .addSyntax("setup")
                .setDescription("Configures the URL of the Message Queue!")
                .setRequiresArgs(ArgsRequired.REQUIRED)
                .build());
    }

    @Override
    public void run() {
        String url = getArgs();

        if (isValidUrl(url)) {
            configureURL(url);
        } else {
            String dialog = ("It seems like the given URL is not a valid address.\n" +
                    "Do you want to configure it anyway?");

            if (UserInterface.getInstance().getUserConfirmationDialog(dialog)) {
                configureURL(url);
            }
        }
    }

    private boolean isValidUrl(String url) {
        return url.matches("((http://)|(https://))([a-zA-Z]|[0-9]|-|\\.|_|\\+)+:[0-9]+");
    }

    /**
     * Sets the MQ of the MQClient
     *
     * @param url well formed url
     */
    private void configureURL(String url) {
        TestClientRepository.getInstance().configureMQUrl(url);
        UserInterface.printSection("Message Queue URL is configured to:\n" + url);
    }


}
