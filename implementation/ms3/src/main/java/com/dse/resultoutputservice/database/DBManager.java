package com.dse.resultoutputservice.database;

import com.dse.resultoutputservice.datatypes.*;
import com.dse.resultoutputservice.network.converter.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

/**
 * The DBManager class is responsible for the communication between application an database.
 */
public class DBManager {
    private final static Logger log = LoggerFactory.getLogger(DBManager.class);
    private /*final*/ static String databaseUrl = "jdbc:sqlite:../ms3/database/database.db"; //for unit testing final cannot be used
    private final static DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendInstant(3).toFormatter();

    /**
     * The insertAnomaly method always inserts the anomaly object at the database table anomalies.
     *
     * @param anomaly object which should be inserted
     * @return true if the object is inserted
     */
    public static boolean insertAnomaly(Anomaly anomaly) {
        boolean result = false;
        try {
            Connection connection = DriverManager.getConnection(databaseUrl);
            Statement statement = connection.createStatement();
            result = statement.execute("insert into anomalies(timestamp, errorType, errorDescription) values('"
                    + formatter.format(anomaly.getTimestamp()) + "', '"
                    + anomaly.getErrorType() + "', '"
                    + anomaly.getErrorDescription() + "');");
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        log.info("Anomaly in database inserted!");
        return result;
    }

    /**
     * The insertAirPrediction method always inserts the airPrediction object at the database table airPredictions.
     *
     * @param airPrediction object which should be inserted
     * @return true if the object is inserted
     */
    public static boolean insertAirPrediction(AirPrediction airPrediction) {
        boolean result = false;
        try {
            Connection connection = DriverManager.getConnection(databaseUrl);
            Statement statement = connection.createStatement();
            result = statement.execute("insert into airPredictions(timestamp, airTemperature, windSpeed, humidity) values('"
                    + formatter.format(airPrediction.getTimestamp()) + "', "
                    + airPrediction.getAirTemperature() + ", "
                    + airPrediction.getWindSpeed() + ", "
                    + airPrediction.getHumidity() + ");");
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        log.info("AirPrediction in database inserted!");
        return result;
    }

    /**
     * The insertWaterPrediction method always inserts the waterPrediction object at the database table waterPredictions.
     *
     * @param waterPrediction object which should be inserted
     * @return true if the object is inserted
     */
    public static boolean insertWaterPrediction(WaterPrediction waterPrediction) {
        boolean result = false;
        try {
            Connection connection = DriverManager.getConnection(databaseUrl);
            Statement statement = connection.createStatement();
            result = statement.execute("insert into waterPredictions(timestamp, waterTemperature, turbidity, waveHeight) values('"
                    + formatter.format(waterPrediction.getTimestamp()) + "', "
                    + waterPrediction.getWaterTemperature() + ", "
                    + waterPrediction.getTurbidity() + ", "
                    + waterPrediction.getWaveHeight() + ");");
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        log.info("WaterPrediction in database inserted!");
        return result;
    }

    /**
     * The queryAnomalies method executes the given sql statement at the database.
     * A converter converts the result always into an anomalies object.
     * So the sql statements should always be select statements from the anomalies table of the database.
     *
     * @param sql statement which will be executed by the method/database
     * @return an anomalies object
     */
    public static Anomalies queryAnomalies(String sql) {
        Anomalies anomalies = null;
        try {
            Connection connection = DriverManager.getConnection(databaseUrl);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            anomalies = Converter.databaseResultSetToAnomalies(resultSet);
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        log.info("Anomalies from database queried!");
        return anomalies;
    }

    /**
     * The queryAirPredictions method executes the given sql statement at the database.
     * A converter converts the result always into an airPredictions object.
     * So the sql statements should always be select statements from the airPredictions table of the database.
     *
     * @param sql statement which will be executed by the method/database
     * @return an airPredictions object
     */
    public static AirPredictions queryAirPredictions(String sql) {
        AirPredictions airPredictions = null;
        try {
            Connection connection = DriverManager.getConnection(databaseUrl);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            airPredictions = Converter.databaseResultSetToAirPredictions(resultSet);
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        log.info("AirPredictions from database queried!");
        return airPredictions;
    }

    /**
     * The queryWaterPredictions method executes the given sql statement at the database.
     * A converter converts the result always into an waterPredictions object.
     * So the sql statements should always be select statements from the waterPredictions table of the database.
     *
     * @param sql statement which will be executed by the method/database
     * @return an waterPredictions object
     */
    public static WaterPredictions queryWaterPredictions(String sql) {
        WaterPredictions waterPredictions = null;
        try {
            Connection connection = DriverManager.getConnection(databaseUrl);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            waterPredictions = Converter.databaseResultSetToWaterPredictions(resultSet);
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        log.info("WaterPredictions from database queried!");
        return waterPredictions;
    }

}
