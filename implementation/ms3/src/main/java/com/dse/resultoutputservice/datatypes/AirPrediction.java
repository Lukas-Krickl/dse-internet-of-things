package com.dse.resultoutputservice.datatypes;

import java.time.Instant;

/**
 * The AirPrediction class is used for micro service internal information handling.
 * This class simulates a table row of the database table airPredictions.
 * So the messages can be converted into airPrediction and easily inserted into the database.
 */
public class AirPrediction {
    private Instant timestamp;
    private double airTemperature;
    private double windSpeed;
    private double humidity;

    public AirPrediction(Instant timestamp, double airTemperature, double windSpeed, double humidity) {
        this.timestamp = timestamp;
        this.airTemperature = airTemperature;
        this.windSpeed = windSpeed;
        this.humidity = humidity;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public double getAirTemperature() {
        return airTemperature;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public double getHumidity() {
        return humidity;
    }
}
