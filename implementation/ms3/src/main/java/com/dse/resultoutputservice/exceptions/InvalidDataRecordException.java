package com.dse.resultoutputservice.exceptions;

public class InvalidDataRecordException extends RuntimeException {
    public InvalidDataRecordException(String message) {
        super(message);
    }
}
