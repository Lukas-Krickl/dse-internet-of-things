package com.dse.resultoutputservice.network.webcommunication;

import com.dse.resultoutputservice.database.DBManager;
import com.dse.resultoutputservice.datatypes.AirPredictions;
import com.dse.resultoutputservice.datatypes.Anomalies;
import com.dse.resultoutputservice.datatypes.WaterPredictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

/**
 * The WebRestController class provides the endpoints for the javascript requests from the browser.
 */
@RestController
public class WebRestController {
    private final static Logger log = LoggerFactory.getLogger(WebRestController.class);

    /**
     * The getAnomalies method serves the browser with an anomalies object.
     * Therefore a sql statement will be generated out of the delivered parameters.
     * The parameters sent by the browser are necessary to inform the server which data is already sent.
     *
     * @param size         the number of anomalies which should be sent to the browser
     * @param maxTimestamp the timestamp from the latest anomaly which is already available at the browser
     * @param minTimestamp the timestamp from the oldest anomaly which is already available at the browser
     * @return an anomalies object
     */
    @GetMapping("getAnomalies")
    public Anomalies getAnomalies(@RequestParam String size, @RequestParam Instant maxTimestamp, @RequestParam Instant minTimestamp) {
        log.info("Anomalies requested!");
        String sql;
        DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendInstant(3).toFormatter();
        if (!maxTimestamp.equals(Instant.parse("1970-01-01T00:00:00.000Z"))) {
            sql = "select * from (select * from anomalies order by timestamp desc limit "
                    + size + ") latestAnomalies where timestamp > '"
                    + formatter.format(maxTimestamp) + "' or timestamp < '"
                    + formatter.format(minTimestamp) + "';";
        } else {
            sql = "select * from anomalies order by timestamp desc limit " + size + ";";
        }
        return DBManager.queryAnomalies(sql);
    }

    /**
     * The getAirPredictions method serves the browser with an airPredictions object.
     * Therefore a sql statement will be generated out of the delivered parameters.
     * The parameters sent by the browser are necessary to inform the server which data is already sent.
     *
     * @param size         the number of airPredictions which should be sent to the browser
     * @param maxTimestamp the timestamp from the airPrediction which is already available at the browser
     * @param minTimestamp the timestamp from the airPrediction which is already available at the browser
     * @return an airPredictions object
     */
    @GetMapping("getAirPredictions")
    public AirPredictions getAirPredictions(@RequestParam String size, @RequestParam Instant maxTimestamp, @RequestParam Instant minTimestamp) {
        log.info("AirPredictions requested!");
        String sql;
        DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendInstant(3).toFormatter();
        if (!maxTimestamp.equals(Instant.parse("1970-01-01T00:00:00.000Z"))) {
            sql = "select * from (select * from airPredictions order by timestamp desc limit "
                    + size + ") latestAirPredictions where timestamp > '"
                    + formatter.format(maxTimestamp) + "' or timestamp < '"
                    + formatter.format(minTimestamp) + "';";
        } else {
            sql = "select * from airPredictions order by timestamp desc limit " + size + ";";
        }
        return DBManager.queryAirPredictions(sql);
    }

    /**
     * The getWaterPredictions method serves the browser with an waterPredictions object.
     * Therefore a sql statement will be generated out of the delivered parameters.
     * The parameters sent by the browser are necessary to inform the server which data is already sent.
     *
     * @param size         the number of waterPredictions which should be sent to the browser
     * @param maxTimestamp the timestamp from the latest waterPrediction which is already available at the browser
     * @param minTimestamp the timestamp from the oldest waterPrediction which is already available at the browser
     * @return an waterPredictions object
     */
    @GetMapping("getWaterPredictions")
    public WaterPredictions getWaterPredictions(@RequestParam String size, @RequestParam Instant maxTimestamp, @RequestParam Instant minTimestamp) {
        log.info("WaterPredictions requested!");
        String sql;
        DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendInstant(3).toFormatter();
        if (!maxTimestamp.equals(Instant.parse("1970-01-01T00:00:00.000Z"))) {
            sql = "select * from (select * from waterPredictions order by timestamp desc limit "
                    + size + ") latestWaterPredictions where timestamp > '"
                    + formatter.format(maxTimestamp) + "' or timestamp < '"
                    + formatter.format(minTimestamp) + "';";
        } else {
            sql = "select * from waterPredictions order by timestamp desc limit " + size + ";";
        }
        return DBManager.queryWaterPredictions(sql);
    }

}
