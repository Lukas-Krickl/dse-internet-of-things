package com.dse.machinelearningservice.logic.anomalydetection;

import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeuemodel.*;

import java.math.BigDecimal;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestClientException;

/**
 * Checks air sensor values for anomalies.
 */
@Slf4j
public class AirSensorAnomalyDetectionComponent extends AnomalyDetectionComponent
{
    private AirSensorValue record;
    private AirSensorPredictionDataRecord prediction;
    private MessageQueueClient messageQueueClient;


    //hard-coded limits for valid entries
    private static final BigDecimal airTemperatureLowerLimit = BigDecimal.valueOf(-5);
    private static final BigDecimal airTemperatureUpperLimit = BigDecimal.valueOf(30);
    private static final BigDecimal windSpeedLowerLimit = BigDecimal.valueOf(0);
    private static final BigDecimal windSpeedUpperLimit = BigDecimal.valueOf(10);
    private static final BigDecimal humidityLowerLimit = BigDecimal.valueOf(20);
    private static final BigDecimal humidityUpperLimit = BigDecimal.valueOf(90);

    //deviation from predicted values
    private static final BigDecimal airTemperatureMaxDeviation = BigDecimal.valueOf(10);
    private static final BigDecimal windSpeedMaxDeviation = BigDecimal.valueOf(5);
    private static final BigDecimal humidityMaxDeviation = BigDecimal.valueOf(10);


    /**
     * Set up the data the anomaly detection component should work with.
     * @param record the air sensor values that should be checked for anomalies
     * @param prediction the current prediction for air sensor values
     * @param messageQueueClient client for communicating with the message queue
     */
    public void setUp(DataRecord record, DataRecord prediction, MessageQueueClient messageQueueClient) {
        this.record = (AirSensorValue) record;
        this.prediction = (AirSensorPredictionDataRecord) prediction;
        this.messageQueueClient = messageQueueClient;
    }

    private void sendAnomalyMessage(ErrorTypeEnum errorType, String description){
        //create record that describes the issue
        AnomalyDataRecord anomalyDataRecord = new AnomalyDataRecord(errorType, description);
        //create a message containing the record
        Message anomalyMessage = new Message(TopicEnum.ANOMALY, anomalyDataRecord);
        //info log
        log.info("Sending anomaly message: Error type is " + anomalyDataRecord.getErrorType() + ", error message is " + anomalyDataRecord.getErrorDescription());

        //send message
        //for local tests messageQueueClient can be set to null in the constructor
        try {
            if (messageQueueClient != null) messageQueueClient.publishMessage(anomalyMessage);
        } catch(RestClientException e) {
            log.info("Could not send message");
            e.printStackTrace();
        }
    }

    /**
     * Check if the sensor values data record contains any null values.
     * @return the number of null values that were found
     */
    public int findNullValueAnomaly() {
        int nullValuesFound = 0;

        //check all fields and send a separate message for each one that is null
        //return the count of null values that were found
        if (record.getAirTemperature() == null) {
            sendAnomalyMessage(ErrorTypeEnum.NULL, "Air temperature at sensor " + record.getWeatherStation() + " is null");
            nullValuesFound++;
        }
        if (record.getHumidity() == null) {
            sendAnomalyMessage(ErrorTypeEnum.NULL, "Humidity at sensor " + record.getWeatherStation() + " is null");
            nullValuesFound++;
        }
        if (record.getWindSpeed() == null) {
            sendAnomalyMessage(ErrorTypeEnum.NULL, "Wind speed at sensor " + record.getWeatherStation() + " is null");
            nullValuesFound++;
        }

        return nullValuesFound;
    }


    /**
     * Check if the received values are outside of the hard-coded bounds.
     * @return the number of entries that are outside the bounds
     */
    public int findOutOfBoundsAnomaly() {
        int outOfBoundsValuesFound = 0;

        if (record.getAirTemperature().compareTo(airTemperatureLowerLimit) == -1 || record.getAirTemperature().compareTo(airTemperatureUpperLimit) == 1 ) {
            sendAnomalyMessage(ErrorTypeEnum.VALUEOFFLIMIT, "Air temperature at sensor " + record.getWeatherStation() + " is " +
                    "out of bounds");
            outOfBoundsValuesFound++;
        }
        if (record.getHumidity().compareTo(humidityLowerLimit) == -1 || record.getHumidity().compareTo(humidityUpperLimit) == 1 ) {
            sendAnomalyMessage(ErrorTypeEnum.VALUEOFFLIMIT, "Humidity at sensor " + record.getWeatherStation() + " is out of bounds");
            outOfBoundsValuesFound++;
        }
        if (record.getWindSpeed().compareTo(windSpeedLowerLimit) == -1 || record.getWindSpeed().compareTo(windSpeedUpperLimit) == 1 ) {
            sendAnomalyMessage(ErrorTypeEnum.VALUEOFFLIMIT, "Wind speed at sensor " + record.getWeatherStation() + " is out of bounds");
            outOfBoundsValuesFound++;
        }
        return outOfBoundsValuesFound;
    }


    /**
     * Check if the sensor values deviate strongly from the current prediction.
     * @return the number of entries in the data record that deviate from the prediction
     */
    public int findPredictionAnomaly() {
        int predictionAnomaliesFound = 0;

        if (record.getAirTemperature().subtract(prediction.getAirTemperature()).abs().compareTo(airTemperatureMaxDeviation) == 1) {
            sendAnomalyMessage(ErrorTypeEnum.PREDICTION, "Air temperature at sensor " + record.getWeatherStation() +
                            " deviates from predicted value");
            predictionAnomaliesFound++;
        }

        if (record.getHumidity().subtract(prediction.getHumidity()).abs().compareTo(humidityMaxDeviation) == 1) {
            sendAnomalyMessage(ErrorTypeEnum.PREDICTION, "Humidity at sensor " + record.getWeatherStation() +
                    " deviates from predicted value");
            predictionAnomaliesFound++;
        }

        if (record.getWindSpeed().subtract(prediction.getWindSpeed()).abs().compareTo(windSpeedMaxDeviation) == 1) {
            sendAnomalyMessage(ErrorTypeEnum.PREDICTION, "Wind speed at sensor " + record.getWeatherStation() +
                    " deviates from predicted value");
            predictionAnomaliesFound++;
        }

        return predictionAnomaliesFound;
    }



    @Override
    public void run() {
        log.info("Air sensor anomaly detection started on thread " + Thread.currentThread().getId());

        int nullValuesFound = findNullValueAnomaly();

        //if there are null values in the data, there's no need to continue
        if (nullValuesFound > 0) return;

        findOutOfBoundsAnomaly();

        if (prediction != null) { //prediction is null if no prediction for the sensortype has been created yet
            findPredictionAnomaly();
        }
    }
}
