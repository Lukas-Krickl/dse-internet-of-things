package com.dse.machinelearningservice.model;

import com.dse.messagequeuemodel.Message;
import com.dse.messagequeuemodel.DataRecord;
import com.dse.messagequeuemodel.TopicEnum;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Thread-safe implementation of MessageRepository interface.
 * All methods that return lists return a shallow copy of the internally stored data. Therefore, if a thread uses
 * such a list to work with, it will not be aware of any changes made to the repository after its shallow copy has
 * been created.
 * For performance reasons, the repository will only store up to 30 messages. When the 30th message is added to the
 * repository, the oldest 10 messages will be deleted.
 */
@Slf4j
public class SimpleMessageRepository implements MessageRepository
{
    private List<Message> messages;

    public SimpleMessageRepository() {
        this.messages = new ArrayList<Message>();
    }

    @Override
    public synchronized int addMessage(Message message) {
        messages.add(message);

        log.info("Adding new message");

        //for performance reasons, we never store more than 30 messages
        if (messages.size() == 30) {
            log.info("Shrinking message repository");

            //delete the first element 10 times
            for (int i = 0; i < 10; ++i) messages.remove(0);
        }

        return messages.size();
    }

    @Override
    public synchronized List<Message> getAllMessages() {
        //streaming and collecting simply to create a shallow copy
        return messages.stream().collect(Collectors.toList());
    }

    @Override
    public List<Message> getMessagesByTopic(TopicEnum topic) {
        List<Message> messageList = messages.stream()
                .filter(message -> message.getTopic().equals(topic))
                .collect(Collectors.toList());
        return messageList;
    }

    @Override
    public synchronized List<DataRecord> getAllDataRecords() {
        List<DataRecord> recordsList = messages.stream().map(message -> message.getRecord()).collect(Collectors.toList());
        return recordsList;
    }

    @Override
    public synchronized List<DataRecord> getDataRecordsByTopic(TopicEnum topic) {
        List<DataRecord> recordsList = messages.stream()
                .filter(message -> message.getTopic().equals(topic))
                .map(message -> message.getRecord())
                .collect(Collectors.toList());
        return recordsList;
    }

    @Override
    public synchronized int getMessageCountByTopic(TopicEnum topic) {
        List<Message> messageList = messages.stream()
                .filter(message -> message.getTopic().equals(topic))
                .collect(Collectors.toList());
        return messageList.size();
    }

    @Override
    public synchronized DataRecord getMostRecentDataRecord() {
        if (messages.isEmpty()) return null;

        Message mostRecentMessage = messages.get(0);
        for (Message message: messages){
            if (message.getTimestamp().isAfter(mostRecentMessage.getTimestamp())) mostRecentMessage = message;
        }

        return mostRecentMessage.getRecord();
    }
}
