package com.dse.iotsimulationservice.aggregator;

import com.dse.messagequeuemodel.AggregableValue;

import java.util.List;

/**
 * Interface for a class that allows to aggregate a List of AggregableValues to a single value
 */
public interface Aggregator {

    /**
     * Aggregates a list of values to a single value using a suitable aggregation method
     *
     * @param values the values to be aggregated
     * @return a single value as an aggregation of all others
     */
    AggregableValue aggregateValues(List<AggregableValue> values);

}
