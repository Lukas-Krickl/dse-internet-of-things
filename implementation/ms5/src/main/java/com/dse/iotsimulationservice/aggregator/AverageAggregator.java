package com.dse.iotsimulationservice.aggregator;

import com.dse.messagequeuemodel.AggregableValue;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;

/**
 * Aggregates a list of values by averaging each value field
 */
@Component
@ConditionalOnProperty(name = "sensor.aggregation.strategy", havingValue = "average")
public class AverageAggregator implements Aggregator {


    /**
     * Aggregates the input list by averaging over the fields "value1", "value2" and "value3".
     * For type, identifier and timestamp the first non null value in the list is used, if none is found,
     * an IllegalArgumentException is thrown
     *
     * @param values the values to be aggregated
     * @return the aggregated value
     */
    @Override
    public AggregableValue aggregateValues(List<AggregableValue> values) {
        BigDecimal value1 = BigDecimal.ZERO;
        long value1Count = 0;
        BigDecimal value2 = BigDecimal.ZERO;
        long value2Count = 0;
        BigDecimal value3 = BigDecimal.ZERO;
        long value3Count = 0;

        AggregableValue srcValue = values.stream()
                .filter(val -> Objects.nonNull(val)
                        && val.getIdentifier() != null
                        && val.getTimeStamp() != null)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("no non-null element in list"));


        if (values.isEmpty()) {
            throw new IllegalArgumentException("No elements in list to be aggregated");
        }

        for (AggregableValue aggregableValue : values) {
            //values can be null, bc filter does not remove them but sets them null on filtering
            if (aggregableValue.getValue1() != null) {
                value1 = value1.add(aggregableValue.getValue1());
                value1Count++;
            }
            if (aggregableValue.getValue2() != null) {
                value2 = value2.add(aggregableValue.getValue2());
                value2Count++;
            }
            if (aggregableValue.getValue3() != null) {
                value3 = value3.add(aggregableValue.getValue3());
                value3Count++;
            }
        }

        value1 = value1Count > 0 ?
                value1.divide(BigDecimal.valueOf(value1Count), 4, RoundingMode.HALF_DOWN).stripTrailingZeros() :
                BigDecimal.ZERO;
        value2 = value2Count > 0 ?
                value2.divide(BigDecimal.valueOf(value2Count), 4, RoundingMode.HALF_DOWN).stripTrailingZeros() :
                BigDecimal.ZERO;
        value3 = value3Count > 0 ?
                value3.divide(BigDecimal.valueOf(value3Count), 4, RoundingMode.HALF_DOWN).stripTrailingZeros() :
                BigDecimal.ZERO;

        return AggregableValue.builder()
                .type(srcValue.getType())
                .identifier(srcValue.getIdentifier())
                .timeStamp(srcValue.getTimeStamp())
                .value1(value1)
                .value2(value2)
                .value3(value3)
                .build();
    }
}
