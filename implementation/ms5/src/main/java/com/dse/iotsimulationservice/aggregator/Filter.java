package com.dse.iotsimulationservice.aggregator;

import com.dse.messagequeuemodel.AggregableValue;

import java.util.List;

public interface Filter {
    List<AggregableValue> filterValues(List<AggregableValue> values);
}
