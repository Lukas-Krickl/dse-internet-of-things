package com.dse.iotsimulationservice.config;

import com.dse.messagequeueclient.MessageQueueClient;
import com.dse.messagequeueclient.MessageQueueClientReceiveController;
import com.dse.messagequeueclient.MessageReceiver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration of the MessageQueueclient
 */
@Configuration
public class MessageQueueClientConfiguration {

    @Bean
    public MessageQueueClient messageQueueClient(
            @Value("${messagequeue.client.clienturl}") String clientUrl,
            @Value("${messagequeue.client.messagequeueurl}") String messageQueueUrl
    ) {
        return new MessageQueueClient(clientUrl, messageQueueUrl);
    }

    @Bean
    public MessageQueueClientReceiveController messageQueueClientReceiveController(MessageReceiver messageReceiver) {
        return new MessageQueueClientReceiveController(messageReceiver);
    }
}
