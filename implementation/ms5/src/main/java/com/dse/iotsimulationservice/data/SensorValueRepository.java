package com.dse.iotsimulationservice.data;

import com.dse.messagequeuemodel.AggregableValue;

import java.util.List;

public interface SensorValueRepository {

    void addValue(AggregableValue sensorValue);

    List<AggregableValue> getAllCachedValues();

    int getValueCacheSize();

    void clearCache();

}
