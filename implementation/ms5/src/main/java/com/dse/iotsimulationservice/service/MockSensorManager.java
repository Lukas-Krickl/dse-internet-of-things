package com.dse.iotsimulationservice.service;

import com.dse.iotsimulationservice.data.SensorValueRepository;
import com.dse.messagequeuemodel.AggregableValue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Responsible for adding omitted values to the repository and invoking a value publication
 * after enough values have been omitted
 */
@Service
public class MockSensorManager implements SensorManager {

    /**
     * Storage for the cached values
     */
    private final SensorValueRepository sensorValueSensorValueRepository;

    /**
     * Service to publish values to the MQ
     */
    private final ValuePublishingService valuePublishingService;

    /**
     * Number of values which need to be omitted until a publishement occurs
     */
    @Value("${sensor.aggregation.size}")
    private int noOfValuesToBeAggregated;

    public MockSensorManager(ValuePublishingService valuePublishingService, SensorValueRepository sensorValueSensorValueRepository) {
        this.valuePublishingService = valuePublishingService;
        this.sensorValueSensorValueRepository = sensorValueSensorValueRepository;
    }

    /**
     * Called by an observable after a value is omitted.
     * Adds the value to the repository and calls the valuePublishingService if a certain configurable threshold is reached
     *
     * @param aggregableValue the value that was omitted.
     */
    @Override
    public void valueOmitted(AggregableValue aggregableValue) {
        sensorValueSensorValueRepository.addValue(aggregableValue);
        if (sensorValueSensorValueRepository.getValueCacheSize() >= noOfValuesToBeAggregated) {
            this.valuePublishingService.publishValues();
        }
    }
}
