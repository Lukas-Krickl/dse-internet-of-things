package com.dse.iotsimulationservice.service.exceptions;

public class InvalidMessageTypeException extends RuntimeException {
    public InvalidMessageTypeException(String message) {
        super(message);
    }
}