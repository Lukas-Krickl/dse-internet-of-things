package com.dse.iotsimulationservice.aggregator;

import com.dse.messagequeuemodel.AggregableValue;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class AverageAggregatorTest {
    private List<AggregableValue> testValues;

    private AverageAggregator aggregator = new AverageAggregator();


    @Test
    void itShouldAggregateValues() {
        testValues = TestFixtures.getAggregableList();
        AggregableValue result = aggregator.aggregateValues(testValues);
        assertThat(result).isNotNull();
        assertThat(result)
                .extracting("value1", "value2", "value3")
                .containsOnly(BigDecimal.valueOf(2));
    }

    @Test
    void itShouldIgnoreNullValues() {
        testValues = TestFixtures.getAggregableListWithNull();
        AggregableValue result = aggregator.aggregateValues(testValues);
        assertThat(result).isNotNull();
        assertThat(result)
                .extracting("value1", "value2", "value3")
                .containsExactly(BigDecimal.valueOf(2.5), BigDecimal.valueOf(2), BigDecimal.valueOf(1.5));
    }

    @Test
    void itShouldBreakOnNullObject() {
        testValues = TestFixtures.getAggregableListWithIdenticalNullField();
        AggregableValue result = aggregator.aggregateValues(testValues);
        assertThat(result).isNotNull();
        assertThat(result)
                .extracting("value1", "value2", "value3")
                .containsExactly(BigDecimal.ZERO, BigDecimal.valueOf(2), BigDecimal.valueOf(2));
    }


}